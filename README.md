# README #

### What is this repository for? ###

* This project represents shop (console program - interaction through command line and web application with simple UI)
* Version - 1.0.1

### How do I get set up? ###

* Java version - 13
* Database configuration - file local.properties has all necessary dependencies which can be overwritten
  (postgreSQL template). By default, project uses HSQLDB (in-memory mode)
* How to run tests - with maven commands (mvn test, mvn verify), project has plugin which show code coverage percentage

### Who do I talk to? ###

* Repo owner or admin