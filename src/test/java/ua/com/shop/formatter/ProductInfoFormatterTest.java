package ua.com.shop.formatter;

import org.junit.jupiter.api.Test;
import ua.com.shop.model.Product;
import ua.com.shop.model.ProductInfo;
import ua.com.shop.model.ProductStatus;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ProductInfoFormatterTest {

    @Test
    void formatShouldReturnFormattedString() {
        Product product = Product.builder()
                .id(7L)
                .price(500)
                .name("ps")
                .status(ProductStatus.IN_STOCK)
                .build();
        ProductInfo productInfo = new ProductInfo(product, 5);

        String result = ProductInfoFormatter.format(
                Collections.singletonList(productInfo)
        );

        assertEquals("Product id | Product name | Product price | Product status | " +
                "Total ordered quantity\n" +
                "7          | ps           | 500           | IN_STOCK       | 5\n", result);
    }
}