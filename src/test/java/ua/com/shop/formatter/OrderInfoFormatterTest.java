package ua.com.shop.formatter;

import org.junit.jupiter.api.Test;
import ua.com.shop.model.OrderInfo;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class OrderInfoFormatterTest {

    @Test
    void formatShouldReturnFormattedString() {
        OrderInfo orderInfo = OrderInfo.builder()
                .orderId(8L)
                .productName("onePlus")
                .quantity(5)
                .totalSum(2500)
                .createdAt(LocalDateTime.parse("2021-02-07T11:11:45.537738")).build();

        String result = OrderInfoFormatter.format(
                Collections.singletonList(orderInfo)
        );

        assertEquals("Order id | Total sum | Product name | Quantity | " +
                "Order date created at\n" +
                "8        | 2500      | onePlus      | 5        | 2021-02-07 11:11\n", result);
    }
}