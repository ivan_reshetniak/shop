package ua.com.shop.formatter;

import org.junit.jupiter.api.Test;
import ua.com.shop.model.Product;
import ua.com.shop.model.ProductStatus;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ProductFormatterTest {

    @Test
    void formatShouldReturnFormattedString() {
        String result = ProductFormatter.format(
                Collections.singletonList(Product.builder()
                        .price(1000).name("test").status(ProductStatus.IN_STOCK).build()
                ));

        assertEquals("Product price | Product name | Product status\n" +
                "1000          | test         | IN_STOCK\n", result);
    }
}