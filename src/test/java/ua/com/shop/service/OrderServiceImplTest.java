package ua.com.shop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ua.com.shop.dao.OrderDaoImpl;
import ua.com.shop.model.Order;
import ua.com.shop.model.OrderInfo;
import ua.com.shop.model.Product;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderServiceImplTest {

    private OrderServiceImpl orderService;
    private ProductServiceImpl productService = mock(ProductServiceImpl.class);
    private OrderDaoImpl orderDao = mock(OrderDaoImpl.class);

    @BeforeEach
    void setUp() {
        orderService = new OrderServiceImpl();
        orderService.setProductService(productService);
        orderService.setOrderDao(orderDao);
    }

    @Test
    void getAllShouldReturnsOrderList() {
        List<Order> orders = Arrays.asList(new Order(), new Order());

        when(orderService.getAll()).thenReturn(orders);

        assertEquals(2, orderService.getAll().size());
    }

    @Test
    void addOrderShouldCallAddOrder() {
        Map<Integer, Integer> productAndQuantity = new HashMap<>();
        productAndQuantity.put(2, 7);

        Product product = Product.builder()
                .id(1L)
                .name("test")
                .build();

        when(productService.getProductById(anyInt())).thenReturn(product);
        doNothing().when(orderDao).addOrder(any());

        orderService.addOrder(productAndQuantity);

        verify(productService, times(1)).getProductById(anyInt());
        verify(orderDao, times(1)).addOrder(any());
    }

    @Test
    void getOrderById_ValidId_shouldReturnOrder() {
        Order order = Order.builder().id(5).build();

        when(orderDao.getOrderById(anyInt())).thenReturn(order);

        assertEquals(5, orderService.getOrderById(1).getId());
    }

    @Test
    void getOrdersInformationShouldReturnOrderInfoList() {
        List<OrderInfo> orderInfos = Arrays.asList(new OrderInfo(), new OrderInfo());

        when(orderDao.getOrdersInformation()).thenReturn(orderInfos);

        assertEquals(2, orderService.getOrdersInformation().size());
    }
}