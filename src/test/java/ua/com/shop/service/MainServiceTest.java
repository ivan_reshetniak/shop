package ua.com.shop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class MainServiceTest {

    private MainService mainService;
    private OrderServiceImpl orderService = mock(OrderServiceImpl.class);
    private ProductServiceImpl productService = mock(ProductServiceImpl.class);

    @BeforeEach
    void setUp() {
        mainService = new MainService();
        mainService.setOrderService(orderService);
        mainService.setProductService(productService);
    }

    @Test
    void addTestDataShouldCallServices() {
        doNothing().when(orderService).addOrder(anyMap());
        doNothing().when(productService).addProduct(anyString(), anyInt(), any());

        mainService.addTestData();

        verify(productService, times(3)).addProduct(anyString(), anyInt(), any());
        verify(orderService, times(2)).addOrder(anyMap());
    }
}