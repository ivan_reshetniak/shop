package ua.com.shop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ua.com.shop.dao.ProductDaoImpl;
import ua.com.shop.model.Product;
import ua.com.shop.model.ProductInfo;
import ua.com.shop.model.ProductStatus;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductServiceImplTest {

    private ProductServiceImpl productService;
    private ProductDaoImpl mockDao = mock(ProductDaoImpl.class);

    @BeforeEach
    void setUp() {
        productService = new ProductServiceImpl();
        productService.setProductDao(mockDao);
    }

    @Test
    void getProductByIdShouldReturnProduct() {
        Product product = Product.builder()
                .id(1L)
                .name("test")
                .price(1000)
                .status(ProductStatus.IN_STOCK)
                .createdAt(LocalDateTime.now())
                .build();

        when(mockDao.getProductById(anyInt())).thenReturn(product);

        assertEquals(1000, productService.getProductById(5).getPrice());
    }

    @Test
    void getAllShouldReturnListProducts() {
        List<Product> products = Arrays.asList(new Product(), new Product(), new Product());

        when(mockDao.getAll()).thenReturn(products);

        assertEquals(3, productService.getAll().size());
    }

    @Test
    void getAllProductsWithTotalOrderedQuantityShouldReturnProductInfoList() {
        List<ProductInfo> productInfos = Arrays.asList(new ProductInfo(), new ProductInfo());

        when(mockDao.getAllProductsWithTotalOrderedQuantity()).thenReturn(productInfos);

        assertEquals(2, productInfos.size());
    }

    @Test
    void deleteProductByIdShouldCallDelete() {
        doNothing().when(mockDao).delete(anyInt());

        productService.deleteProduct(5);
        verify(mockDao, times(1)).delete(anyInt());
    }

    @Test
    void deleteAllShouldCallDelete() {
        doNothing().when(mockDao).deleteAll();

        productService.deleteAll();
        verify(mockDao, times(1)).deleteAll();
    }

    @Test
    void addProductShouldCallAddProduct() {
        doNothing().when(mockDao).addProduct(any());

        productService.addProduct("test", 1000, ProductStatus.IN_STOCK);
        verify(mockDao, times(1)).addProduct(any());
    }
}