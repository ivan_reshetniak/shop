package ua.com.shop.servlet;

import ua.com.shop.model.Order;
import ua.com.shop.model.OrderInfo;
import ua.com.shop.model.Product;
import ua.com.shop.service.OrderService;
import ua.com.shop.service.OrderServiceImpl;
import ua.com.shop.service.ProductService;
import ua.com.shop.service.ProductServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrderServlet extends HttpServlet {

    private final OrderService orderService = new OrderServiceImpl();
    private final ProductService productService = new ProductServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher;
        String createOrderParam = req.getParameter("createOrder");
        String additionalInfoParam = req.getParameter("additionalInfo");

        if (createOrderParam != null && createOrderParam.equals("true")) {
            Map<Product, Integer> productAndQuantity = new HashMap<>();
            List<Product> products = productService.getAll();
            products.forEach(product -> productAndQuantity.put(product, 0));

            req.setAttribute("productAndQuantity", productAndQuantity);
            dispatcher = getServletContext().getRequestDispatcher("/createOrder.jsp");
            dispatcher.forward(req, resp);
        } else if (additionalInfoParam != null && additionalInfoParam.equals("true")) {
            List<OrderInfo> orderInfos = orderService.getOrdersInformation();

            req.setAttribute("orderInfos", orderInfos);
            dispatcher = getServletContext().getRequestDispatcher("/orderInfo.jsp");
            dispatcher.forward(req, resp);
        } else {
            List<Order> orders = orderService.getAll();

            req.setAttribute("orders", orders);
            dispatcher = getServletContext().getRequestDispatcher("/orders.jsp");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<Integer, Integer> productIdAndQuantity = new HashMap<>();

        List<String> idOfProducts = productService.getAll().stream()
                .map(p -> String.valueOf(p.getId()))
                .collect(Collectors.toList());

        idOfProducts.forEach(id -> {
            String parameterValue = req.getParameter(id);
            if (parameterValue != null && !parameterValue.equals("0")) {
                productIdAndQuantity.put(Integer.parseInt(id), Integer.parseInt(parameterValue));
            }
        });
        orderService.addOrder(productIdAndQuantity);
        resp.sendRedirect("/orders");
    }
}
