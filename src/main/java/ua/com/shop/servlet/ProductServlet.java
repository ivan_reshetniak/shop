package ua.com.shop.servlet;

import ua.com.shop.model.Product;
import ua.com.shop.model.ProductInfo;
import ua.com.shop.model.ProductStatus;
import ua.com.shop.service.ProductService;
import ua.com.shop.service.ProductServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ProductServlet extends HttpServlet {

    private final ProductService productService = new ProductServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String additionalInfo = req.getParameter("additionalInfo");
        RequestDispatcher dispatcher;
        if (additionalInfo != null) {
            List<ProductInfo> productInfos = productService.getAllProductsWithTotalOrderedQuantity();

            req.setAttribute("productInfos", productInfos);
            dispatcher = getServletContext().getRequestDispatcher("/productInfos.jsp");
            dispatcher.forward(req, resp);
        } else {
            List<Product> products = productService.getAll();

            req.setAttribute("products", products);
            dispatcher = getServletContext().getRequestDispatcher("/products.jsp");
            dispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String delete = req.getParameter("delete");
        if (id != null) {
            productService.deleteProduct(Integer.parseInt(id));
        } else if (delete != null) {
            productService.deleteAll();
        } else {
            String name = req.getParameter("name");
            int price = Integer.parseInt(req.getParameter("price"));
            ProductStatus status = ProductStatus.get(Integer.parseInt(req.getParameter("status")));

            productService.addProduct(name, price, status);
        }
        resp.sendRedirect("/products");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String productId = req.getParameter("id");
        productService.deleteProduct(Integer.parseInt(productId));

        resp.sendRedirect("/products");
    }
}
