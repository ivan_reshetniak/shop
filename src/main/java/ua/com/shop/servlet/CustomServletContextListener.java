package ua.com.shop.servlet;

import ua.com.shop.service.MainService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class CustomServletContextListener implements ServletContextListener {

    private static final MainService mainService = new MainService();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        mainService.addTestData();
    }
}
