package ua.com.shop.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.Properties;

public class DataBaseConnection {

    private static DataBaseConnection connection = null;
    private final EntityManagerFactory emf;
    private final EntityManager em;

    private DataBaseConnection() {
        emf = Persistence.createEntityManagerFactory("JPA", loadProperties());
        em = emf.createEntityManager();
    }

    public static EntityManager getEntityManager() {
        if (connection == null) {
            connection = new DataBaseConnection();
        }
        return connection.em;
    }

    public static Properties loadProperties() {
        Properties prop = new Properties();
        try {
            prop.load(DataBaseConnection.class.getClassLoader().getResourceAsStream("local.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
